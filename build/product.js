"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.product_router = void 0;
const express_1 = __importDefault(require("express"));
exports.product_router = express_1.default.Router();
const connection_1 = require("./connection");
exports.product_router.use(express_1.default.json());
class productInfo {
    constructor() {
        this.product_id = 0;
        this.product_name = "";
        this.product_image = "";
        this.product_description = "";
        this.product_price = 0;
        this.discount = 0;
        this.discount_type = "";
        this.category_id = 0;
        this.category_name = "";
        this.total_amount_after_discount = 0;
    }
}
;
// show product all information.
exports.product_router.get("/get", (req, res) => {
    const qry = `
    select p.*,c.category_name from product as p
    inner join category as c on p.category_id=c.category_id`;
    connection_1.cN.query(qry, (err, result) => {
        if (err) {
            res.status(500).send({ error: err.sqlMessage });
        }
        var list_productinfo = [];
        for (let item of result) {
            const productinformation = {
                product_id: item.product_id,
                product_name: item.product_name,
                product_image: item.product_image,
                product_description: item.product_description,
                product_price: item.product_price,
                discount: item.discount,
                discount_type: item.discount_type,
                category_id: item.category_id,
                category_name: item.category_name,
            };
            if (item.discount_type == "%") {
                productinformation["total_amount_after_discount"] = Math.round(item.product_price - (item.product_price * (item.discount / 100)));
                list_productinfo.push(productinformation);
            }
            else {
                productinformation["total_amount_after_discount"] = Math.round(item.product_price - item.discount);
                list_productinfo.push(productinformation);
            }
        }
        return res.status(200).send(list_productinfo);
    });
});
//show product information by product id.
exports.product_router.get("/get/product/:product_id", (req, res) => {
    const qry = `
    select p.*,c.category_name from product as p
    inner join category as c on p.category_id=c.category_id where p.product_id=?`;
    connection_1.cN.query(qry, req.params.product_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        const productinformation = {
            product_id: result[0].product_id,
            product_name: result[0].product_name,
            product_image: result[0].product_image,
            product_description: result[0].product_description,
            product_price: result[0].product_price,
            discount: result[0].discount,
            discount_type: result[0].discount_type,
            category_id: result[0].category_id,
            category_name: result[0].category_name,
        };
        if (productinformation.discount_type == "%") {
            productinformation["total_amount_after_discount"] = Math.round(productinformation.product_price - (productinformation.product_price * (productinformation.discount / 100)));
        }
        else {
            productinformation["total_amount_after_discount"] = Math.round(productinformation.product_price - productinformation.discount);
        }
        return res.status(200).send(productinformation);
    });
});
// show product information by category
exports.product_router.get("/get/category/:category_name", (req, res) => {
    const qry = `
    select p.*,c.category_name from product as p
    inner join category as c on p.category_id=c.category_id where c.category_name=?`;
    connection_1.cN.query(qry, req.params.category_name, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        var list_productinfo = [];
        for (let item of result) {
            const productinformation = {
                product_id: item.product_id,
                product_name: item.product_name,
                product_image: item.product_image,
                product_description: item.product_description,
                product_price: item.product_price,
                discount: item.discount,
                discount_type: item.discount_type,
                category_id: item.category_id,
                category_name: item.category_name,
            };
            if (item.discount_type == "%") {
                productinformation["total_amount_after_discount"] = Math.round(item.product_price - (item.product_price * (item.discount / 100)));
                list_productinfo.push(productinformation);
            }
            else {
                productinformation["total_amount_after_discount"] = Math.round(item.product_price - item.discount);
                list_productinfo.push(productinformation);
            }
        }
        return res.status(200).send(list_productinfo);
    });
});
// add product information.
exports.product_router.post("/post", (req, res) => {
    const data = req.body;
    if ((typeof data.product_name == "string" && typeof data.product_image == "string" && typeof data.product_description == "string" && typeof data.product_price == "number" && typeof data.discount == "number" && typeof data.discount_type == "string" && typeof data.category_id == "number") == false) {
        return res.status(400).send({ error: "syntax error" });
    }
    const qry = `insert into product(product_name,product_image,product_description,product_price,discount,discount_type,category_id) values(?,?,?,?,?,?,?)`;
    connection_1.cN.query(qry, [data.product_name, data.product_image, data.product_description, data.product_price, data.discount, data.discount_type, data.category_id], (err, result) => {
        if (err) {
            if (err.sqlMessage.slice(0, 9) == "Duplicate") {
                return res.status(400).send({ error: "Duplicate entry" });
            }
            return res.status(500).send({ error: "internal error" });
        }
        return res.status(200).send({ message: "user data added" });
    });
});
// update product inforamtion by using product_id.
exports.product_router.put("/put/:product_id", (req, res) => {
    const data = req.body;
    if ((typeof data.product_name == "string" && typeof data.product_image == "string" && typeof data.product_description == "string" && typeof data.product_price == "number" && typeof data.discount == "number" && typeof data.discount_type == "string" && typeof data.category_id == "number") == false) {
        return res.status(400).send({ error: "syntax error" });
    }
    const qry = "select * from product where product_id=?";
    connection_1.cN.query(qry, req.params.product_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        if (result.length == 0) {
            return res.status(400).send({ error: "please enter valid user_id" });
        }
        const qry = "update product set product_name=?,product_image=?,product_description=?,product_price=?,discount=?,discount_type=?,category_id=? where product_id=?";
        connection_1.cN.query(qry, [data.product_name, data.product_image, data.product_description, data.product_price, data.discount, data.discount_type, data.category_id, req.params.product_id], (err, result) => {
            if (err) {
                if (err.sqlMessage.slice(0, 9) == "Duplicate") {
                    return res.status(400).send({ error: "Duplicate entry" });
                }
                return res.status(500).send({ error: "internal error" });
            }
            return res.status(200).send({ message: "data will be updated" });
        });
    });
});
// delete product information by using .
exports.product_router.delete("/delete/:product_id", (req, res) => {
    const qry = "select * from product where product_id=?";
    connection_1.cN.query(qry, req.params.product_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        if (result.length == 0) {
            return res.status(400).send({ error: "please enter valid user_id" });
        }
        const qry = "delete from product where product_id=?";
        connection_1.cN.query(qry, req.params.product_id, (err, result) => {
            if (err) {
                return res.status(500).send({ error: err.sqlMessage });
            }
            return res.status(200).send({ message: "data will be deleted" });
        });
    });
});
