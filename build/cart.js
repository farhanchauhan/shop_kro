"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cart_router = void 0;
const express_1 = __importDefault(require("express"));
exports.cart_router = express_1.default.Router();
const connection_1 = require("./connection");
// import {decodedRequest} from "./authentication"
exports.cart_router.use(express_1.default.json());
class cart_product_Info {
    constructor() {
        this.product_name = "";
        this.product_image = "";
        this.product_description = "";
        this.product_price = 0;
        this.discount = 0;
        this.discount_type = "";
        this.product_id = 0;
        this.cart_id = 0;
        this.quantity = 0;
        this.total_amount_after_discount = 0;
        this.total_amount_of_product_with_quantity = 0;
    }
}
class cart_Info {
    constructor() {
        this.user_name = "";
        this.total_product = 0;
        this.product_list = [];
        this.total_price_of_all_product = 0;
    }
}
;
// show information of cart.
exports.cart_router.get("/get", (req, res) => {
    const user_id = res.locals.decode.user_id;
    const qry = `
    select * from cart as c
    inner join user as u on c.user_id = u.user_id
    inner join product as p on c.product_id=p.product_id
    where u.user_id=?`;
    connection_1.cN.query(qry, user_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        if (result.length == 0) {
            return res.status(500).send({ error: "empty" });
        }
        var list_cartinfo = [];
        var total_amount = 0;
        for (let item of result) {
            const cart_product_information = {
                cart_id: item.cart_id,
                product_id: item.product_id,
                product_name: item.product_name,
                product_image: item.product_image,
                product_description: item.product_description,
                product_price: item.product_price,
                discount: item.discount,
                discount_type: item.discount_type,
                quantity: item.quantity
            };
            if (item.discount_type == "%") {
                cart_product_information["total_amount_after_discount"] = item.product_price - (item.product_price * (item.discount / 100));
                cart_product_information["total_amount_of_product_with_quantity"] = Math.round((item.product_price - (item.product_price * (item.discount / 100))) * item.quantity);
                total_amount += Math.round((item.product_price - (item.product_price * (item.discount / 100))) * item.quantity);
                list_cartinfo.push(cart_product_information);
            }
            else {
                cart_product_information["total_amount_after_discount"] = item.product_price - item.discount;
                cart_product_information["total_amount_of_product_with_quantity"] = Math.round((item.product_price - item.discount) * item.quantity);
                total_amount += Math.round((item.product_price - item.discount) * item.quantity);
                list_cartinfo.push(cart_product_information);
            }
        }
        const cart_information = { user_name: result[0].user_name, total_product: result.length, product_list: list_cartinfo, total_price_of_all_product: total_amount };
        return res.status(200).send(cart_information);
    });
});
// add cart detail.
exports.cart_router.post("/post", (req, res) => {
    const user_id = res.locals.decode.user_id;
    const data = req.body;
    if ((typeof data.product_id == "number" && typeof data.quantity == "number") == false) {
        return res.status(400).send({ error: "syntax error" });
    }
    const qry = `insert into cart(user_id,product_id,quantity) values(?,?,?)`;
    connection_1.cN.query(qry, [user_id, data.product_id, data.quantity], (err, result) => {
        if (err) {
            if (err.sqlMessage.slice(0, 9) == "Duplicate") {
                return res.status(400).send({ error: "Duplicate entry" });
            }
            return res.status(500).send({ error: "internal error" });
        }
        return res.status(200).send({ message: "cart data added" });
    });
});
// update cart detail.
exports.cart_router.put("/put", (req, res) => {
    const user_id = res.locals.decode.user_id;
    const data = req.body;
    if ((typeof data.quantity == "number" && typeof data.product_id == "number") == false) {
        return res.status(400).send({ error: "syntax error" });
    }
    const qry = "update cart set quantity=? where  user_id=? and product_id=? ";
    connection_1.cN.query(qry, [data.quantity, user_id, data.product_id], (err, result) => {
        if (err) {
            if (err.sqlMessage.slice(0, 9) == "Duplicate") {
                return res.status(400).send({ error: "Duplicate entry" });
            }
            return res.status(500).send({ error: "internal error" });
        }
        return res.status(200).send({ message: "data will be updated" });
    });
});
// delete cart detail.
exports.cart_router.delete("/delete", (req, res) => {
    const user_id = res.locals.decode.user_id;
    const data = req.body;
    if ((typeof data.product_id == "number" && typeof data.cart_id == "number") == false) {
        return res.status(400).send({ error: "syntax_error" });
    }
    const qry = "delete from cart where user_id=? and  product_id=? and cart_id=?";
    connection_1.cN.query(qry, [user_id, data.product_id, data.cart_id], (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        return res.status(299).send({ message: "data will be deleted" });
    });
});
