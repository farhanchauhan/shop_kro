"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cancel_router = void 0;
const express_1 = __importDefault(require("express"));
exports.cancel_router = (0, express_1.default)();
exports.cancel_router.get("/", (req, res) => {
    res.send("cancel");
});
