"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cN = void 0;
const mysql2_1 = __importDefault(require("mysql2"));
exports.cN = mysql2_1.default.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "shop_kro"
});
exports.cN.connect((err) => {
    if (err) {
        console.log(err);
    }
    else {
        console.log("-->: connected with database ");
    }
});
//module.exports=cN;
