"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.user_router = void 0;
const express_1 = __importDefault(require("express"));
exports.user_router = express_1.default.Router();
const connection_1 = require("./connection");
exports.user_router.use(express_1.default.json());
class userInfo {
    constructor() {
        this.user_id = 0;
        this.user_name = "";
        this.email = "";
        this.mobile_number = "";
        this.password = "";
    }
}
;
;
exports.user_router.get("/get", (req, res) => {
    const qry = `select * from user`;
    connection_1.cN.query(qry, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        var list_userinfo = [];
        for (let item of result) {
            const userinformation = {
                user_id: item.user_id,
                user_name: item.user_name,
                email: item.email,
                mobile_number: item.mobile_number,
                password: item.password
            };
            list_userinfo.push(userinformation);
        }
        return res.status(200).send(list_userinfo);
    });
});
exports.user_router.post("/post", (req, res) => {
    const data = req.body;
    if ((typeof data.user_name == "string" && typeof data.email == "string" && typeof data.mobile_number == "string" && typeof data.password == "string") == false) {
        return res.status(400).send({ error: "syntax error" });
    }
    const emailValidation = /([A-z a-z 0-9]+[@][a-z]+[.][a-z])/;
    if (emailValidation.test(data.email) == false) {
        return res.status(400).send({ error: "please inter valid email id" });
    }
    const qry = `insert into user(user_name,email,mobile_number,password) values(?,?,?,?)`;
    connection_1.cN.query(qry, [data.user_name, data.email, data.mobile_number, data.password], (err, result) => {
        if (err) {
            if (err.sqlMessage.slice(0, 9) == "Duplicate") {
                return res.status(400).send({ error: "Duplicate entry" });
            }
            return res.status(500).send({ error: "internal error" });
        }
        return res.status(200).send({ message: "user data added" });
    });
});
exports.user_router.put("/put/:user_id", (req, res) => {
    const data = req.body;
    if ((typeof data.user_name == "string" && typeof data.email == "string" && typeof data.mobile_number == "string" && typeof data.password == "string") == false) {
        return res.status(400).send({ error: "syntax error" });
    }
    const qry = "select * from user where user_id=?";
    connection_1.cN.query(qry, req.params.user_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        if (result.length == 0) {
            return res.status(400).send({ error: "please enter valid user_id" });
        }
        const qry = "update user set user_name=?,email=?,mobile_number=?,password=? where user_id=?";
        connection_1.cN.query(qry, [data.user_name, data.email, data.mobile_number, data.password, req.params.user_id], (err, result) => {
            if (err) {
                if (err.sqlMessage.slice(0, 9) == "Duplicate") {
                    return res.status(400).send({ error: "Duplicate entry" });
                }
                return res.status(500).send({ error: "internal error" });
            }
            return res.status(200).send({ message: "data will be updated" });
        });
    });
});
exports.user_router.delete("/delete/:user_id", (req, res) => {
    const qry = "select * from user where user_id=?";
    connection_1.cN.query(qry, req.params.user_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        if (result.length == 0) {
            return res.status(400).send({ error: "please enter valid user_id" });
        }
        const qry = "delete from user where user_id=?";
        connection_1.cN.query(qry, req.params.user_id, (err, result) => {
            if (err) {
                return res.status(500).send({ error: err.sqlMessage });
            }
            return res.status(200).send({ message: "data will be deleted" });
        });
    });
});
