"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.middleware = exports.authRouter = void 0;
// import Express,{Request,Response} from "express";
//const Express=require("express")
const express_1 = __importDefault(require("express"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const authRoutercN = require("./connection");
const connection_1 = require("./connection");
exports.authRouter = express_1.default.Router();
exports.authRouter.use(express_1.default.json());
const middleware = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    let token = req.headers.authorization;
    if (token == undefined) {
        return res.status(200).send({ error: "token is not present" });
    }
    if (token.startsWith("Bearer ")) {
        token = token.slice(7, token.length);
    }
    if (token) {
        const decodedToken = jsonwebtoken_1.default.verify(token, "thisismysecret");
        res.locals.decode = decodedToken;
        return next();
    }
    return res.status(400).send({ success: "false", message: "token is note right" });
});
exports.middleware = middleware;
exports.authRouter.post("/post", (req, res) => {
    const body = req.body;
    const qry = `select * from user`;
    connection_1.cN.query(qry, (err, result) => {
        if (err) {
            res.status(500).send({ error: err.sqlMessage });
        }
        for (const item of result) {
            if (body.email == item.email && body.password == item.password) {
                var token = jsonwebtoken_1.default.sign({ user_id: item.user_id, role: "admin" }, "thisismysecret", { expiresIn: "160h" });
                return res.status(200).send({ token: "Bearer " + token });
            }
            continue;
        }
        return res.status(400).send({ error: "please enter valid emal and password" });
    });
});
exports.authRouter.get("/get", exports.middleware, (req, res) => {
    return res.status(200).send({ message: "login successfull" });
});
