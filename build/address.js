"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.address_router = void 0;
const express_1 = __importDefault(require("express"));
const connection_1 = require("./connection");
exports.address_router = express_1.default.Router();
exports.address_router.use(express_1.default.json());
// show address information of perticular user
exports.address_router.get("/get", (req, res) => {
    const user_id = res.locals.decode.user_id;
    const qry = `select * from address where user_id=?`;
    connection_1.cN.query(qry, user_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        if (result.length == 0) {
            return res.status(400).send({ error: "address note present please add address" });
        }
        const lst_of_address_info = [];
        for (let val of result) {
            const address_info = {
                address_id: val.address_id,
                user_id: user_id,
                country: val.country,
                full_name: val.full_name,
                street_number: val.street_number,
                city: val.city,
                state: val.state,
                pincode: val.pincode,
                mobile_number: val.mobile_number
            };
            lst_of_address_info.push(address_info);
        }
        return res.send(lst_of_address_info);
    });
});
// add address detail of perticular user.
exports.address_router.post("/post", (req, res) => {
    const data = req.body;
    const user_id = res.locals.decode.user_id;
    if ((typeof data.country == "string" && typeof data.full_name == "string" && typeof data.street_number == "string" && typeof data.city == "string" && typeof data.state == "string" && typeof data.pincode == "string" && typeof data.mobile_number == "string") == false) {
        return res.status(400).send({ error: "Syntax error" });
    }
    const qry = `
    insert into address(user_id,country,full_name,street_number,city,state,pincode,mobile_number)
    values(?,?,?,?,?,?,?,?)
    `;
    connection_1.cN.query(qry, [user_id, data.country, data.full_name, data.street_number, data.city, data.state, data.pincode, data.mobile_number], (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        return res.status(200).send({ message: "address data added" });
    });
});
//change address of users
exports.address_router.put("/put/:address_id", (req, res) => {
    const data = req.body;
    const user_id = res.locals.decode.user_id;
    const address_id = Number(req.params.address_id);
    if ((typeof address_id == "number" && typeof data.country == "string" && typeof data.full_name == "string" && typeof data.street_number == "string" && typeof data.city == "string" && typeof data.state == "string" && typeof data.pincode == "string" && typeof data.mobile_number == "string") == false) {
        return res.status(400).send({ error: "Syntax error" });
    }
    const qry = `update address set country=?,full_name=?,street_number=?,city=?,state=?,pincode=?,mobile_number=?
    where user_id=? and address_id=?`;
    connection_1.cN.query(qry, [data.country, data.full_name, data.street_number, data.city, data.state, data.pincode, data.mobile_number, user_id, address_id], (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        return res.status(200).send({ message: "address data updated" });
    });
});
exports.address_router.delete("/delete", (req, res) => {
    const user_id = res.locals.decode.user_id;
    const data = req.body;
    const qry = `delete from address where address_id=? and user_id`;
    connection_1.cN.query(qry, [data.address_id, user_id], (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        return res.status(200).send({ message: "deleted" });
    });
});
