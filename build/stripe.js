"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.stripe_router = void 0;
const express_1 = __importDefault(require("express"));
const stripe_1 = __importDefault(require("stripe"));
const connection_1 = require("./connection");
const stripe = new stripe_1.default('sk_test_51OZTC5SG6bGIPC6CBLy0bXeeatDAQAfoFzT8NZ3p6MRVicKYWudCDTIXZzc0g07mMn9Rjn0wf70Hpf7cX5ZBalAM005VsV0HTs');
exports.stripe_router = express_1.default.Router();
exports.stripe_router.use(express_1.default.static('public'));
exports.stripe_router.use(express_1.default.json());
exports.stripe_router.post("/post", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const total_price = req.body.total_amount;
    var total_amount_product = 0;
    const user_id = res.locals.decode.user_id;
    const qry = `select * from cart as c
    inner join product as p on p.product_id=c.product_id
    where user_id=?`;
    const list_of_product_info = [];
    connection_1.cN.query(qry, user_id, (err, result) => __awaiter(void 0, void 0, void 0, function* () {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        for (let item of result) {
            const product_info = {
                product_name: item.product_name,
                quantity: item.quantity
            };
            if (item.discount_type == "%") {
                product_info["total_amount_after_discount"] = (item.product_price - (item.product_price * (item.discount / 100))) * 100;
                list_of_product_info.push(product_info);
                continue;
            }
            else {
                product_info["total_amount_after_discount"] = (item.product_price - item.discount) * 100;
                list_of_product_info.push(product_info);
                continue;
            }
        }
        const data = list_of_product_info;
        const lineItems = data.map((values) => ({
            price_data: {
                currency: "inr",
                product_data: {
                    name: values.product_name
                },
                unit_amount: values.total_amount_after_discount
            },
            quantity: values.quantity
        }));
        const session = yield stripe.checkout.sessions.create({
            line_items: lineItems,
            payment_method_types: ['card',],
            mode: 'payment',
            success_url: "http://localhost:3000/success",
            cancel_url: "http://localhost:3000/cancel",
            customer_email: 'customer@example.com', // Add customer's email
            billing_address_collection: 'required',
            shipping_address_collection: {
                allowed_countries: ['IN'], // Specify the allowed countries for shipping address
            },
        });
        return res.json({ id: session.id });
    }));
}));
