"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.review_router = void 0;
const express_1 = __importDefault(require("express"));
exports.review_router = express_1.default.Router();
const connection_1 = require("./connection");
exports.review_router.use(express_1.default.json());
exports.review_router.get("/get", (req, res) => {
    var total_amount_product = 0;
    const user_id = res.locals.decode.user_id;
    const qry = `select * from review as v 
    inner join cart as c on c.user_id=v.user_id
    inner join product as p on p.product_id=c.product_id
    inner join address as adr on adr.address_id=v.address_id
    where v.user_id=?`;
    const list_of_product_info = [];
    connection_1.cN.query(qry, user_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        if (result.length == 0) {
            return res.status(400).send({ message: "data is enpty" });
        }
        for (let item of result) {
            const product_info = {
                product_name: item.product_name,
                product_description: item.product_description,
                quantity: item.quantity,
            };
            if (item.discount_type == "%") {
                product_info["total_amount"] = item.quantity * (item.product_price - (item.product_price * (item.discount / 100)));
                total_amount_product += item.quantity * (item.product_price - (item.product_price * (item.discount / 100)));
                list_of_product_info.push(product_info);
                continue;
            }
            else {
                product_info["total_amount"] = item.quantity * (item.product_price - item.discount);
                total_amount_product += (item.quantity * (item.product_price - item.discount));
                list_of_product_info.push(product_info);
                continue;
            }
        }
        const address_information = {
            full_name: result[0].full_name,
            street_number: result[0].street_number,
            city: result[0].city,
            state: result[0].state,
            pincode: result[0].pincode,
            country: result[0].country,
            mobile_number: result[0].mobile_number
        };
        return res.status(200).send({ Totoal_product: result.length, Product_information: list_of_product_info, Address_information: address_information, total_amount_products: total_amount_product });
    });
});
//add data in review router.
exports.review_router.post("/post", (req, res) => {
    const data = req.body;
    const user_id = res.locals.decode.user_id;
    const qry = `insert into review(user_id,address_id)
    values(?,?)`;
    connection_1.cN.query(qry, [user_id, data.address_id], (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        return res.status(200).send({ message: "data added" });
    });
});
// delete address
exports.review_router.delete("/delete", (req, res) => {
    const user_id = res.locals.decode.user_id;
    const qry = `delete from review where user_id=? and address_id=?`;
    connection_1.cN.query(qry, [user_id, req.body.address_id], (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        return res.status(200).send({ message: "done" });
    });
});
