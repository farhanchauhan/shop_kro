"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.product_recomondation_router = void 0;
const express_1 = __importDefault(require("express"));
exports.product_recomondation_router = express_1.default.Router();
const connection_1 = require("./connection");
const python_shell_1 = require("python-shell");
exports.product_recomondation_router.use(express_1.default.json());
class productInfo {
    constructor() {
        this.product_id = 0;
        this.product_name = "";
        this.product_image = "";
        this.product_description = "";
        this.product_price = 0;
        this.discount = 0;
        this.discount_type = "";
        this.category_id = 0;
        this.category_name = "";
        this.total_amount_after_discount = 0;
    }
}
;
exports.product_recomondation_router.get("/get/:product_id", (req, res) => {
    //  var list_of_recomondation:Array<Array<Number>>=[]
    var list_of_recomondation = [];
    // Sample data to be passed from Node.js to Python
    const user_id = res.locals.decode.user_id;
    var input_data = [user_id, req.params.product_id];
    let options = {
        scriptPath: "C:/Users/DESKTOP1/Desktop/GitLab/shop_kro",
        args: input_data.map(String)
    };
    python_shell_1.PythonShell.run('python.py', options).then(results => {
        for (let i = 0; i < results.length - 1; i++) {
            list_of_recomondation[i] = Number(results[i].slice(3));
        }
        const qry = "select * from product where product_id =? or product_id=? or product_id=? or product_id=? or product_id=? or product_id=? or product_id=? or product_id=?";
        connection_1.cN.query(qry, list_of_recomondation, (err, result) => {
            if (err) {
                return res.status(500).send({ error: err.sqlMessage });
            }
            var list_productinfo = [];
            for (let item of result) {
                const productinformation = {
                    product_id: item.product_id,
                    product_name: item.product_name,
                    product_image: item.product_image,
                    product_description: item.product_description,
                    product_price: item.product_price,
                    discount: item.discount,
                    discount_type: item.discount_type,
                    category_id: item.category_id,
                    category_name: item.category_name,
                };
                if (item.discount_type == "%") {
                    productinformation["total_amount_after_discount"] = Math.round(item.product_price - (item.product_price * (item.discount / 100)));
                    list_productinfo.push(productinformation);
                }
                else {
                    productinformation["total_amount_after_discount"] = Math.round(item.product_price - item.discount);
                    list_productinfo.push(productinformation);
                }
            }
            return res.status(200).send(list_productinfo);
        });
    });
});
