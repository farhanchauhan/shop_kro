"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = void 0;
// import liberary
const express_1 = __importDefault(require("express"));
const category_1 = require("./category");
const user_1 = require("./user");
const product_1 = require("./product");
const cart_1 = require("./cart");
const authentication_1 = require("./authentication");
const address_1 = require("./address");
const Buy_now_1 = require("./Buy_now");
const review_1 = require("./review");
const stripe_1 = require("./stripe");
const success_1 = require("./success");
const cancel_1 = require("./cancel");
const product_recomondation_1 = require("./product_recomondation");
// call express application.
exports.app = (0, express_1.default)();
const cors = require("cors");
exports.app.use(cors());
// making routes.
exports.app.use("/category", category_1.category_router);
exports.app.use("/user", user_1.user_router);
exports.app.use("/product", product_1.product_router);
exports.app.use("/cart", authentication_1.middleware, cart_1.cart_router);
exports.app.use("/authentication", authentication_1.authRouter);
exports.app.use("/address", authentication_1.middleware, address_1.address_router);
exports.app.use("/buy", authentication_1.middleware, Buy_now_1.buy_router);
exports.app.use("/review", authentication_1.middleware, review_1.review_router);
exports.app.use("/stripe", authentication_1.middleware, stripe_1.stripe_router);
exports.app.use("/success", success_1.success_router);
exports.app.use("/cancel", cancel_1.cancel_router);
exports.app.use("/product_recomondation", authentication_1.middleware, product_recomondation_1.product_recomondation_router);
//listen on 9000 port.
exports.app.listen(9000, () => {
    console.log("*-* We are Online *-*");
});
