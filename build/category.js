"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.category_router = void 0;
const express_1 = __importDefault(require("express"));
exports.category_router = express_1.default.Router();
const connection_1 = require("./connection");
exports.category_router.use(express_1.default.json());
class categoryInfo {
    constructor() {
        this.category_id = 0;
        this.category_name = "";
        this.category_image = "";
        this.category_description = "";
    }
}
exports.category_router.get("/get", (req, res) => {
    const qry = `select * from category`;
    connection_1.cN.query(qry, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        var list_categoryinfo = [];
        for (let item of result) {
            const categoryinformation = {
                category_id: item.category_id,
                category_name: item.category_name,
                category_image: item.category_image,
                category_description: item.category_description
            };
            list_categoryinfo.push(categoryinformation);
        }
        res.status(200).send(list_categoryinfo);
    });
});
// category detail by id
exports.category_router.get("/get/:category_id", (req, res) => {
    const qry = `select * from category where category_id=?`;
    connection_1.cN.query(qry, req.params.category_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        if (result.length == 0) {
            return res.status(400).send({ error: "please enter valid category_id" });
        }
        const categoryinformation = {
            category_id: result[0].category_id,
            category_name: result[0].category_name,
            category_image: result[0].category_image,
            category_description: result[0].category_description
        };
        res.status(200).send(categoryinformation);
    });
});
exports.category_router.post("/post", (req, res) => {
    const data = req.body;
    if ((typeof data.category_name == "string" && typeof data.category_image == "string" && typeof data.category_description == "string") == false) {
        return res.status(400).send({ error: "syntax error" });
    }
    const qry = `insert into category(category_name,category_image,category_description) values(?,?,?)`;
    connection_1.cN.query(qry, [data.category_name, data.category_image, data.category_description], (err, result) => {
        if (err) {
            if (err.sqlMessage.slice(0, 9) == "Duplicate") {
                return res.status(400).send({ error: "Duplicate entry" });
            }
            return res.status(500).send({ error: "internal error" });
        }
        return res.status(200).send({ message: "user data added" });
    });
});
exports.category_router.put("/put/:category_id", (req, res) => {
    const data = req.body;
    if ((typeof data.category_name == "string" && typeof data.category_image == "string" && typeof data.category_description == "string") == false) {
        return res.status(400).send({ error: "syntax error" });
    }
    const qry = "select * from category where category_id=?";
    connection_1.cN.query(qry, req.params.category_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        if (result.length == 0) {
            return res.status(400).send({ error: "please enter valid user_id" });
        }
        const qry = "update category set category_name=?,category_image=?,category_description=? where category_id=?";
        connection_1.cN.query(qry, [data.category_name, data.category_image, data.category_description, req.params.category_id], (err, result) => {
            if (err) {
                if (err.sqlMessage.slice(0, 9) == "Duplicate") {
                    return res.status(400).send({ error: "Duplicate entry" });
                }
                return res.status(500).send({ error: "internal error" });
            }
            return res.status(200).send({ message: "data will be updated" });
        });
    });
});
exports.category_router.delete("/delete/:category_id", (req, res) => {
    const qry = "select * from category where category_id=?";
    connection_1.cN.query(qry, req.params.category_id, (err, result) => {
        if (err) {
            return res.status(500).send({ error: err.sqlMessage });
        }
        if (result.length == 0) {
            return res.status(400).send({ error: "please enter valid user_id" });
        }
        const qry = "delete from category where category_id=?";
        connection_1.cN.query(qry, req.params.category_id, (err, result) => {
            if (err) {
                return res.status(500).send({ error: err.sqlMessage });
            }
            return res.send({ message: "data will be deleted" });
        });
    });
});
